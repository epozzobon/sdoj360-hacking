CC=powerpc-eabi-gcc
LD=powerpc-eabi-ld

CFLAGS=-I. -mcpu=powerpc64 -mpowerpc64 -mbig -mno-toc -mno-eabi -Os
LDFLAGS= -nodefaultlibs -nostdlib -static
BUILD_DIR=build

SDOJ_PATH?=<Please enter here the path to your SDOJ directory>
UNPACHED_XEX?=$(SDOJ_PATH)/CA022100.bin.bak
XEX_DESTINATION?=$(SDOJ_PATH)/CA022100.bin

.PHONY: dir install clean disasm
all: dir $(BUILD_DIR)/patch.elf $(BUILD_DIR)/patch.hex $(BUILD_DIR)/patch.bin $(BUILD_DIR)/patch.map $(BUILD_DIR)/CA022100.bin

dir:
	mkdir -p $(BUILD_DIR)

$(BUILD_DIR)/patch.o: patch.c sdoj.h
	$(CC) -c -o $@ patch.c $(CFLAGS)

$(BUILD_DIR)/startcode.o: startcode.S
	$(CC) -c -o $@ startcode.S $(CFLAGS)

$(BUILD_DIR)/patch.elf $(BUILD_DIR)/patch.map: patch.ld $(BUILD_DIR)/startcode.o $(BUILD_DIR)/patch.o 
	$(LD) -T patch.ld -o $(BUILD_DIR)/patch.elf $(BUILD_DIR)/startcode.o $(BUILD_DIR)/patch.o -Map=$(BUILD_DIR)/patch.map 

$(BUILD_DIR)/patch.hex: $(BUILD_DIR)/patch.elf
	powerpc-eabi-objcopy -O ihex $(BUILD_DIR)/patch.elf $(BUILD_DIR)/patch.hex
	
$(BUILD_DIR)/patch.bin: $(BUILD_DIR)/patch.elf
	powerpc-eabi-objcopy -O binary $(BUILD_DIR)/patch.elf $(BUILD_DIR)/patch.bin

$(BUILD_DIR)/CA022100.bin: $(BUILD_DIR)/patch.bin
ifeq "$(shell md5sum '$(UNPACHED_XEX)' | cut -d' ' -f 1)" "94b652f80cebd25cb268103b331debc5"
	cp "$(UNPACHED_XEX)" $(BUILD_DIR)/CA022100.bin.tmp
	# Append patch at the end of the executable
	dd if=$(BUILD_DIR)/patch.bin of=$(BUILD_DIR)/CA022100.bin.tmp bs=1 conv=notrunc seek=$(shell printf "%d" 0x194800)
	# Patch the entry point, replacing render_sprite_batch
	# Jumps from 0x88052198 to 0x88192800: BL +0x140668 (0x48140669)
	printf '\x48\x14\x06\x69' | dd of=$(BUILD_DIR)/CA022100.bin.tmp bs=1 count=4 conv=notrunc seek=$(shell printf "%d" 0x54198)
	mv $(BUILD_DIR)/CA022100.bin.tmp $(BUILD_DIR)/CA022100.bin
else
	exit 1
endif

install: $(BUILD_DIR)/CA022100.bin
	cp $(BUILD_DIR)/CA022100.bin "$(XEX_DESTINATION)"

clean:
	rm -rf $(BUILD_DIR)

disasm: $(BUILD_DIR)/patch.elf
	powerpc-eabi-objdump --disassemble-all $(BUILD_DIR)/patch.elf
