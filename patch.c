#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>
#include "sdoj.h"

static uint32_t buttons_state;
static bool render_hitboxes;
static const uint32_t hitbox_hues[24] = {
    0xffff0000, 0xffff0000, 0xff00ff00,          0,
             0,          0,          0,          0,
             0,          0,          0,          0,
             0,          0,          0,          0,
             0,          0,          0,          0,
    0xffffffff,          0,          0,          0,
};

static const uint8_t removed_layers[512] = {
    0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0
};

static int get_input(int gamepad, int button_code) {
    // This call is done through a function pointer cause this function is in another executable
    int (*get_input_ptr)(void *, int, int);
    get_input_ptr = (void *) 0x8217cdd0;
    void *input_driver = *((void **) 0x885f1cdc);
    return get_input_ptr(input_driver, gamepad, button_code);
}

static void setup() {
    // This is only called the first time
    buttons_state = 0;
    render_hitboxes = false;
}

static void draw_hitboxes() {
    struct collider_list_head *head;
    struct collider *c;
    struct sprite_batch_entry *s;
    struct hitbox *h;
    unsigned i;
    uint32_t hue;
    
    for (i = 0; i < 24; i++) {
        hue = hitbox_hues[i];
        if (0 != hue) {
            head = &collider_list_heads[i];
            for (c = head->first; c != NULL; c = c->next) {
                h = c->hitbox;
                uint32_t hb_top, hb_bot, hb_lft, hb_rgt;
                hb_top = hb_bot = h->position->y;
                hb_lft = hb_rgt = h->position->x;
                if (i != 20) {
                    hb_top -= h->top << 16;
                    hb_bot += h->bottom << 16;
                    hb_lft -= h->left << 16;
                    hb_rgt += h->right << 16;
                }

                struct sprite_batch_args sprite_args = {
                    .dest_x = hb_lft >> 16,
                    .dest_y = hb_top >> 16,
                    .height = 32,
                    .width = 32,
                    .src_y = 0x200 - 0x140 - 32,
                    .src_x = 0x1e0,
                    .field_0x1c = 0,
                    .layer = 300
                };


                s = allocate_sprite_batch_entry(0, &sprite_args);
                if (s == NULL) { return; }
                fun_880303b0(s, 4);
                set_sprite_first_nibble(s, 2);
                s->hue = hue;

                sprite_args.dest_y = hb_bot >> 16;
                s = allocate_sprite_batch_entry(0, &sprite_args);
                if (s == NULL) { return; }
                fun_880303b0(s, 4);
                set_sprite_first_nibble(s, 0);
                s->hue = hue;

                sprite_args.dest_x = hb_rgt >> 16;
                s = allocate_sprite_batch_entry(0, &sprite_args);
                if (s == NULL) { return; }
                fun_880303b0(s, 4);
                set_sprite_first_nibble(s, 1);
                s->hue = hue;

                sprite_args.dest_y = hb_top >> 16;
                s = allocate_sprite_batch_entry(0, &sprite_args);
                if (s == NULL) { return; }
                fun_880303b0(s, 4);
                set_sprite_first_nibble(s, 3);
                s->hue = hue;
            }
        }
    }
}

int on_frame() {

    if (*frame_index == 0) {
        setup();
    }

    uint32_t back_button_pressed = get_input(0, 5);
    uint32_t left_thumb_pressed = get_input(0, 6);
    uint32_t right_thumb_pressed = get_input(0, 7);

    // Compare previous buttons state to current pressed button
    // If back button is newly pressed, toggle hitboxes:
    if (back_button_pressed && !(buttons_state & 0x20)) {
        render_hitboxes = !render_hitboxes;
    }

    buttons_state = (0
        | (back_button_pressed ? 0x00000020 : 0)
        | (left_thumb_pressed  ? 0x00000040 : 0)
        | (right_thumb_pressed ? 0x00000080 : 0)
    );

    uint32_t render_inhibited = *((uint32_t *) 0x8887919c);
    if (render_hitboxes && !render_inhibited) {
        draw_hitboxes();
    }
}
