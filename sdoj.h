#pragma once
#include <stdint.h>

struct __attribute__((__packed__)) vector2 {
    int32_t x;
    int32_t y;
};

struct __attribute__((__packed__)) hitbox {
    struct hitbox *next;
    uint32_t field_0x4;
    uint32_t field_0x8;
    uint32_t field_0xc;
    uint32_t field_0x10;
    uint32_t field_0x14;
    uint32_t field_0x18;
    uint32_t field_0x1c;
    uint32_t field_0x20;
    uint32_t right;
    uint32_t left;
    uint32_t top;
    uint32_t bottom;
    uint32_t field_0x34;
    uint32_t field_0x38;
    uint32_t field_0x3c;
    struct vector2 *position;
    uint32_t field_0x44;
    uint32_t field_0x48;
    uint32_t field_0x4c;
    uint32_t field_0x50;
    uint32_t field_0x54;
    uint32_t field_0x58;
    uint32_t field_0x5c;
    uint32_t field_0x60;
    uint32_t field_0x64;
    uint32_t field_0x68;
    uint32_t field_0x6c;
    uint32_t field_0x70;
    uint32_t field_0x74;
    uint32_t field_0x78;
};

struct __attribute__((__packed__)) collider {
    struct collider *next;
    struct collider *prev;
    uint32_t enabled;
    uint32_t field_0xc;
    uint32_t field_0x10;
    uint32_t field_0x14;
    uint32_t field_0x18;
    struct hitbox *hitbox;
    uint32_t function;
    uint32_t field_0x24;
};

struct __attribute__((__packed__)) collider_list_head {
    int32_t num;
    struct collider *first;
    struct collider *last;
};

struct __attribute__((__packed__)) sprite_batch_args {
    int32_t dest_y;
    int32_t dest_x;
    uint32_t layer;
    uint32_t src_y;
    uint32_t src_x;
    uint32_t height;
    uint32_t width;
    uint32_t field_0x1c;
};

struct __attribute__((__packed__)) sprite_batch_entry {
    uint32_t field_0x0;
    int16_t dest_y;
    int16_t dest_x;
    uint32_t field_0x8;
    uint32_t field_0xc;
    uint32_t field_0x10;
    uint32_t field_0x14;
    uint32_t field_0x18;
    uint32_t src_y;
    uint32_t src_x;
    uint32_t src_height;
    uint32_t src_width;
    uint32_t field_0x2c;
    uint32_t hue;
    uint32_t field_0x34;
    uint32_t field_0x38;
    uint32_t texture;
    uint32_t scale_y;
    uint32_t scale_x;
    uint32_t field_0x48;
    uint32_t field_0x4c;
    uint32_t field_0x50;
};

struct sprite_batch_entry *allocate_sprite_batch_entry(uint32_t texture, struct sprite_batch_args *args);
void fun_880303b0(struct sprite_batch_entry *sprite, uint32_t arg);
struct sprite_batch_entry *append_sprite_batch_entry(uint32_t sprite, int32_t x, int32_t y, uint32_t sprite_render_batch_idx);
void set_sprite_first_nibble(struct sprite_batch_entry *sprite, uint32_t value);
void render_sprite_batch();

extern uint32_t frame_index[1];
extern struct collider_list_head collider_list_heads[24];
